var autoslide = 1;

var erd_start = 619;
var erd_min = 400;
var erd_max = 2000;

var fhc_start = 39.5;
var fhc_min = 20;
var fhc_max = 200;

var flange_start = 71.1;
var flange_min = 20;
var flange_max = 130;

function update_erd(ui){
	if(autoslide) {
		$("#erd").val(parseInt(ui.value));
	}
	calc_spoke_length();
}

function update_fhc(ui){
	if(autoslide) {
		$("#fhc").val(parseInt(ui.value));
	}
	calc_spoke_length();
}

function update_flange(ui){
	if(autoslide) {
		$("#flange").val(parseInt(ui.value));
	}
	calc_spoke_length();
}


function init_sliders() {
	$("#erd-slide").slider("moveTo", erd_start);
	$("#fhc-slide").slider("moveTo", fhc_start);
	$("#flange-slide").slider("moveTo", flange_start);
}

function monitor_inputs() {

	$("#erd").bind("keypress", function(e) {
		if(!onlyNumbers(e) && !onlyFieldNavCommand(e)) {
			return false;
		}
		return true;
	});
	
	$("#erd").bind("keyup", function() {
		if($("#erd").val()) {
			autoslide = 0;	
			$("#erd").val(parseInt($("#erd").val()));
			$("#erd-slide").slider("moveTo", parseInt($("#erd").val()));
			autoslide = 1;
		}
	});
	
	$("#erd-minus").bind("mouseup", function() {
		if($("#erd").val() > erd_min) {
			$("#erd").val(parseInt($("#erd").val())-1);
			autoslide=0;
			$("#erd-slide").slider("moveTo", parseInt($("#erd").val()));
			autoslide=1;
		}
		
	});
	
	$("#erd-plus").bind("mouseup", function() {
		if($("#erd").val() < erd_max) {
			$("#erd").val(parseInt($("#erd").val())+1);
			autoslide=0;
			$("#erd-slide").slider("moveTo", parseInt($("#erd").val()));
			autoslide=1;
		}
	});
	
	
	
	
	

	
	$("#fhc").bind("keypress", function(e) {
		if(!onlyNumbers(e) && !onlyFieldNavCommand(e)) {
			return false;
		}
		return true;
	});
	
	$("#fhc-minus").bind("mouseup", function() {
		if($("#fhc").val() > fhc_min) {
			$("#fhc").val(parseInt($("#fhc").val())-1);
			autoslide=0;
			$("#fhc-slide").slider("moveTo", parseInt($("#fhc").val()));
			autoslide=1;
		}
		
	});
	
	$("#fhc-plus").bind("mouseup", function() {
		if($("#fhc").val() < fhc_max) {
			$("#fhc").val(parseInt($("#fhc").val())+1);
			autoslide=0;
			$("#fhc-slide").slider("moveTo", parseInt($("#fhc").val()));
			autoslide=1;
		}
	});
	
	
	
	

	$("#flange").bind("keypress", function(e) {
		if(!onlyNumbers(e) && !onlyFieldNavCommand(e)) {
			return false;
		}
		return true;
	});
	
	$("#flange-minus").bind("mouseup", function() {
		if($("#flange").val() > flange_min) {
			$("#flange").val(parseInt($("#flange").val())-1);
			autoslide=0;
			$("#flange-slide").slider("moveTo", parseInt($("#flange").val()));
			autoslide=1;
		}
		
	});
	
	$("#flange-plus").bind("mouseup", function() {
		if($("#flange").val() < flange_max) {
			$("#flange").val(parseInt($("#flange").val())+1);
			autoslide=0;
			$("#flange-slide").slider("moveTo", parseInt($("#flange").val()));
			autoslide=1;
		}
	});
	
}

function calc_spoke_length() {
	var flange_spacing = parseFloat($("#flange").val());
	var flange_diameter = parseFloat($("#fhc").val());
	var erd = parseFloat($("#erd").val());
	var cross = parseFloat($("input[name=cross]:checked").val());
	var holes = parseFloat($("input[name=hole]:checked").val());
	var spoke_hole_diameter = 2.5;
	var spoke_length = 0.0;

	if( flange_spacing > 0 &&
		flange_diameter > 0 &&
		erd > 0) {
		
		with(Math) {
			spoke_length = sqrt(
				pow( (flange_diameter/2 * sin(2 * PI * cross / (holes/2)) ), 2 ) +
				pow( erd/2 - ( (flange_diameter/2) * cos(2 * PI * cross / (holes/2)) ), 2 ) +
				pow( flange_spacing/2, 2 )
			) - spoke_hole_diameter/2;
		}

		$("#spoke-length").html("Spoke length = " + Math.round(spoke_length*10)/10 + " mm");
	} else {
		$("#spoke-length").html("Please enter all three measurements.");
	}
}

function update_radios() {
	var theID = $("input[name=cross]:checked").attr("id");
	$("label[for="+theID+"]").addClass("radio-label-selected");
	$("label[for!="+theID+"]").removeClass("radio-label-selected");
	theID = $("input[name=hole]:checked").attr("id");
	$("label[for="+theID+"]").addClass("radio-label-selected");
}

function onlyFieldNavCommand(e)
{
	switch(e.which) {
		case 3:
		case 8:
		case 9:
		case 13:
		case 37:
		case 38:
		case 39:
		case 40:
		case 46:
		case 63232:
		case 63233:
		case 63234:
		case 63235:
		case 63272:
			return true;
	}
	
	switch(e.keyCode) {
		case 3:
		case 8:
		case 9:
		case 13:
		case 37:
		case 38:
		case 39:
		case 40:
		case 46:
			return true;
	}
			
	return false;
}

function onlyNumbers(e)
{
	var keynum = e.which;
	var keychar;
	var numcheck;

	keychar = String.fromCharCode(keynum);
	numcheck = /\d/;
	return numcheck.test(keychar);
}

function validate_numeric_inputs() {
	if(!($("#erd").val > "")) {
		$("#erd").val("1");
	}
}

$().ready(function(){
	$("#erd-slide").slider({
		slide: function(a,ui){
			update_erd(ui);
		},
		change: function(a,ui){
			update_erd(ui);
		},
		min: erd_min,
		max: erd_max
	});
	
	$("#fhc-slide").slider({
		slide: function(a,ui){
			update_fhc(ui);
		},
		change: function(a,ui){
			update_fhc(ui);
		},
		min: fhc_min,
		max: fhc_max
	});
	
	$("#flange-slide").slider({
		slide: function(a,ui){
			update_flange(ui);
		},
		change: function(a,ui){
			update_flange(ui);
		},
		min: flange_min,
		max: flange_max
	});
	
	init_sliders();
	monitor_inputs();
	update_radios();
	
	$("input").change(function(){
		update_radios();
		//validate_numeric_inputs();
		calc_spoke_length();
	});
	
	$("#mainform").submit(function(){
		return false;
	});
	
	
	// work around IE weirdness
	$().bind("click", function(){
		update_radios();
	});
});
